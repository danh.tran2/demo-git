// Create a new list item when clicking on the "Add" button
function newElement() {
  var inputToDo = document.getElementById("myInput").value;
  if (!inputToDo) {
    alert("You must write something!");
  } else {
    const id = new Date().getTime();
    // const newToDoItem = new ItemToDo(id, false, inputToDo);
    const newToDoItem = new ItemToDo(id, false, inputToDo);
    listToDo.push(newToDoItem);
    sessionStorage.setItem("listTD", JSON.stringify(listToDo));
    createList(listToDo);
  }
  document.getElementById("myInput").value = "";
}

function removeToDoItem(id) {
  const index = findIndex(id);
  if (index >= 0) {
    listToDo.splice(index, 1);
    createList(listToDo);
  }
  sessionStorage.setItem("listTD", JSON.stringify(listToDo));
}

//====================
/* 
function createChecked(){
  var list = document.getElementById("myUL");
  var listItem = list.getElementsByTagName("Li");
  // var list = document.querySelectorAll("h2");
  // var list = document.querySelector("ul");
  listItem.addEventListener(
    "click",
    function(ev) {
      if (ev.target.tagName === "LI") {
        ev.target.classList.toggle("checked");
      }
    },
    false
  );
}
*/

function changeStatus(id, obj){
  const index = findIndex(id);
  // console.log(index, listToDo[index].toDoStatus);
  listToDo[index].toDoStatus = !listToDo[index].toDoStatus;
  obj.classList.toggle("checked");
  sessionStorage.setItem("listTD", JSON.stringify(listToDo));
}

//======================================================================
function findIndex(id) {
  for (var i = 0; i < listToDo.length; i++) {
    // listToDo[i].id đang ,là number vì khi lấy id từ example lấy biến index chạy
    if (+listToDo[i].id === +id) {
      return i;
    }
  }
  return -1;
}

function createCheckClass(id) {
  const index = findIndex(id);
  if (listToDo[index].toDoStatus) {
    // classList.add("checked");
    return "checked";
  }
  return "";
}

function createList(listToDo) {
  var i;
  var listContent = "";
  for (i = 0; i < listToDo.length; i++) {
    listContent += ` 
      <li class="${createCheckClass(listToDo[i].id)}" onclick = "changeStatus('${listToDo[i].id}', this)">        
        ${listToDo[i].toDoContent}
        <span class="close" 
            onclick = "removeToDoItem('${listToDo[i].id}')">\u00D7
        </span>
      </li>
    `;
  }
  document.getElementById("myUL").innerHTML = listContent;
}

//*********************************************** */

function createExampleList() {
  var i, status, content;
  var exampleList = document.getElementsByTagName("LI");
  for (i = 0; i < exampleList.length; i++) {
    if (exampleList[i].classList.contains("checked")) {
      status = true;
    } else {
      status = false;
    }
    content = exampleList[i].innerHTML;
    const itemExample = new ItemToDo(i, status, content);
    listToDo.push(itemExample);
  }
  sessionStorage.setItem("listTD", JSON.stringify(listToDo));
}

//======================================================================

function getDataFromSession() {
  const listToDoStr = sessionStorage.getItem("listTD");
  if (listToDoStr) {
    listToDo = JSON.parse(listToDoStr);
  } else {
    createExampleList();
  }
}

//======================================================================

var listToDo = [];
getDataFromSession();
createList(listToDo);
